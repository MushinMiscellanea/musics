---
output:
  word_document: default
  html_document: default
---

# Music Applications

## Spencer Finkel

### Goals:** 
1. Create console applications to assist in music production
2. Programming in Python
4. Current Projects
    1. Transposition Calculator
    2. Compression Release Calculator
    3. Harmonic Frequency Calculator



#### Screenshots:

![Transposition calculator screenshot](png/transpose.png)



![Compression Release calculator screenshot](png/compression.png)



![Harmonic freqeuncy calculator based on root frequency (Hz)](png/harmonics.png)






